<?php

namespace App\Repository;

use App\Entity\TestSeries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TestSeries|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestSeries|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestSeries[]    findAll()
 * @method TestSeries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestSeriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestSeries::class);
    }

    // /**
    //  * @return TestSeries[] Returns an array of TestSeries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestSeries
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
