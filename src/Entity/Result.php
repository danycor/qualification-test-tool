<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 */
class Result
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestCampaign", mappedBy="result")
     */
    private $testCampaigns;

    public function __construct()
    {
        $this->testCampaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|TestCampaign[]
     */
    public function getTestCampaigns(): Collection
    {
        return $this->testCampaigns;
    }

    public function addTestCampaign(TestCampaign $testCampaign): self
    {
        if (!$this->testCampaigns->contains($testCampaign)) {
            $this->testCampaigns[] = $testCampaign;
            $testCampaign->setResult($this);
        }

        return $this;
    }

    public function removeTestCampaign(TestCampaign $testCampaign): self
    {
        if ($this->testCampaigns->contains($testCampaign)) {
            $this->testCampaigns->removeElement($testCampaign);
            // set the owning side to null (unless already changed)
            if ($testCampaign->getResult() === $this) {
                $testCampaign->setResult(null);
            }
        }

        return $this;
    }
}
