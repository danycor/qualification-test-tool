<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestCampaignRepository")
 */
class TestCampaign
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Test", inversedBy="testCampaigns")
     */
    private $test;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Result", inversedBy="testCampaigns")
     */
    private $result;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Campaign", mappedBy="testCampaign")
     */
    private $campaign;


    public function __construct()
    {
        $this->campaign = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function setResult(?Result $result): self
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getCampaign(): Collection
    {
        return $this->campaign;
    }

    public function addCampaign(Campaign $campaign): self
    {
        if (!$this->campaign->contains($campaign)) {
            $this->campaign[] = $campaign;
            $campaign->setTestCampaign($this);
        }

        return $this;
    }

    public function removeCampaign(Campaign $campaign): self
    {
        if ($this->campaign->contains($campaign)) {
            $this->campaign->removeElement($campaign);
            // set the owning side to null (unless already changed)
            if ($campaign->getTestCampaign() === $this) {
                $campaign->setTestCampaign(null);
            }
        }

        return $this;
    }

}
