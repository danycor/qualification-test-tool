<?php

namespace App\Repository;

use App\Entity\TestCampaign;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TestCampaign|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestCampaign|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestCampaign[]    findAll()
 * @method TestCampaign[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestCampaignRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestCampaign::class);
    }

    // /**
    //  * @return TestCampaign[] Returns an array of TestCampaign objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestCampaign
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
