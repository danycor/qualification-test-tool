# Test Projet Qualif


## CMD

* Run local dev server : `php bin/console server:start`
* Create DB php `bin/console doctrine:database:create`
* Make Migration `php bin/console make:migration`
* Apply Migration `php bin/console doctrine:migrations:migrate`
* Run server in local dev : `php bin/console server:run`
* Load fixture `php bin/console doctrine:fixtures:load`
* Cache clear `php bin/console cache:clear`
* Install assets `php bin/console assets:install`
* Create fixtures : `php bin/console make:fixtures`
* Revert migration to 0 : `php bin/console doctrine:migrations:migrate first`
* Migration state : `php bin/console doctrine:migrations:status`

## Typs

* Access to .env value : `$_ENV['DB_USER'];`
* Create DB user : `CREATE USER 'newuser'@'%' IDENTIFIED BY 'password';`
* Grand DB user : `GRANT ALL PRIVILEGES ON qualif.* TO 'newuser'@'%';`

## .env file

```
APP_ENV=prod
APP_SECRET=1
DATABASE_URL=mysql://USR:MDP@127.0.0.1:3306/qualif
MYSQL_ROOT_PASSWORD=MDP
MYSQL_ROOT_HOST=USR

```