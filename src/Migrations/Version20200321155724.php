<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200321155724 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE test_campaign (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, result_id INT DEFAULT NULL, INDEX IDX_8A76198B1E5D0459 (test_id), INDEX IDX_8A76198B7A7B643 (result_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_condition (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE context (id INT AUTO_INCREMENT NOT NULL, parent_context_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_E25D857E2C43459F (parent_context_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE context_condition (context_id INT NOT NULL, condition_id INT NOT NULL, INDEX IDX_6E14885C6B00C1CF (context_id), INDEX IDX_6E14885C887793B6 (condition_id), PRIMARY KEY(context_id, condition_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE context_test_group (context_id INT NOT NULL, test_group_id INT NOT NULL, INDEX IDX_EBC52D3B6B00C1CF (context_id), INDEX IDX_EBC52D3B6B37D211 (test_group_id), PRIMARY KEY(context_id, test_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, context_id INT DEFAULT NULL, number VARCHAR(20) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_D87F7E0C6B00C1CF (context_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_test (test_source INT NOT NULL, test_target INT NOT NULL, INDEX IDX_DD0654C3C853785D (test_source), INDEX IDX_DD0654C3D1B628D2 (test_target), PRIMARY KEY(test_source, test_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feature (id INT AUTO_INCREMENT NOT NULL, subject_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1FD7756623EDC87 (subject_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subject (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campaign (id INT AUTO_INCREMENT NOT NULL, test_campaign_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_1F1512DDA9474CCA (test_campaign_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_group (id INT AUTO_INCREMENT NOT NULL, feature_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_661EE2CE60E4B879 (feature_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE result (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_series (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, execution_date DATE NOT NULL, environnement VARCHAR(255) NOT NULL, status VARCHAR(2) NOT NULL, ticket VARCHAR(7) NOT NULL, INDEX IDX_ADC90603A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_series_campaign (test_series_id INT NOT NULL, campaign_id INT NOT NULL, INDEX IDX_3230BA97D0E5A656 (test_series_id), INDEX IDX_3230BA97F639F774 (campaign_id), PRIMARY KEY(test_series_id, campaign_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE test_campaign ADD CONSTRAINT FK_8A76198B1E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE test_campaign ADD CONSTRAINT FK_8A76198B7A7B643 FOREIGN KEY (result_id) REFERENCES result (id)');
        $this->addSql('ALTER TABLE context ADD CONSTRAINT FK_E25D857E2C43459F FOREIGN KEY (parent_context_id) REFERENCES context (id)');
        $this->addSql('ALTER TABLE context_condition ADD CONSTRAINT FK_6E14885C6B00C1CF FOREIGN KEY (context_id) REFERENCES context (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE context_condition ADD CONSTRAINT FK_6E14885C887793B6 FOREIGN KEY (condition_id) REFERENCES test_condition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE context_test_group ADD CONSTRAINT FK_EBC52D3B6B00C1CF FOREIGN KEY (context_id) REFERENCES context (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE context_test_group ADD CONSTRAINT FK_EBC52D3B6B37D211 FOREIGN KEY (test_group_id) REFERENCES test_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0C6B00C1CF FOREIGN KEY (context_id) REFERENCES context (id)');
        $this->addSql('ALTER TABLE test_test ADD CONSTRAINT FK_DD0654C3C853785D FOREIGN KEY (test_source) REFERENCES test (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_test ADD CONSTRAINT FK_DD0654C3D1B628D2 FOREIGN KEY (test_target) REFERENCES test (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE feature ADD CONSTRAINT FK_1FD7756623EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_1F1512DDA9474CCA FOREIGN KEY (test_campaign_id) REFERENCES test_campaign (id)');
        $this->addSql('ALTER TABLE test_group ADD CONSTRAINT FK_661EE2CE60E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id)');
        $this->addSql('ALTER TABLE test_series ADD CONSTRAINT FK_ADC90603A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE test_series_campaign ADD CONSTRAINT FK_3230BA97D0E5A656 FOREIGN KEY (test_series_id) REFERENCES test_series (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_series_campaign ADD CONSTRAINT FK_3230BA97F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DDA9474CCA');
        $this->addSql('ALTER TABLE context_condition DROP FOREIGN KEY FK_6E14885C887793B6');
        $this->addSql('ALTER TABLE context DROP FOREIGN KEY FK_E25D857E2C43459F');
        $this->addSql('ALTER TABLE context_condition DROP FOREIGN KEY FK_6E14885C6B00C1CF');
        $this->addSql('ALTER TABLE context_test_group DROP FOREIGN KEY FK_EBC52D3B6B00C1CF');
        $this->addSql('ALTER TABLE test DROP FOREIGN KEY FK_D87F7E0C6B00C1CF');
        $this->addSql('ALTER TABLE test_campaign DROP FOREIGN KEY FK_8A76198B1E5D0459');
        $this->addSql('ALTER TABLE test_test DROP FOREIGN KEY FK_DD0654C3C853785D');
        $this->addSql('ALTER TABLE test_test DROP FOREIGN KEY FK_DD0654C3D1B628D2');
        $this->addSql('ALTER TABLE test_group DROP FOREIGN KEY FK_661EE2CE60E4B879');
        $this->addSql('ALTER TABLE feature DROP FOREIGN KEY FK_1FD7756623EDC87');
        $this->addSql('ALTER TABLE test_series_campaign DROP FOREIGN KEY FK_3230BA97F639F774');
        $this->addSql('ALTER TABLE context_test_group DROP FOREIGN KEY FK_EBC52D3B6B37D211');
        $this->addSql('ALTER TABLE test_series DROP FOREIGN KEY FK_ADC90603A76ED395');
        $this->addSql('ALTER TABLE test_campaign DROP FOREIGN KEY FK_8A76198B7A7B643');
        $this->addSql('ALTER TABLE test_series_campaign DROP FOREIGN KEY FK_3230BA97D0E5A656');
        $this->addSql('DROP TABLE test_campaign');
        $this->addSql('DROP TABLE test_condition');
        $this->addSql('DROP TABLE context');
        $this->addSql('DROP TABLE context_condition');
        $this->addSql('DROP TABLE context_test_group');
        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE test_test');
        $this->addSql('DROP TABLE feature');
        $this->addSql('DROP TABLE subject');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE test_group');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE result');
        $this->addSql('DROP TABLE test_series');
        $this->addSql('DROP TABLE test_series_campaign');
    }
}
