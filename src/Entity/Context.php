<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContextRepository")
 */
class Context
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Context", inversedBy="childContexts")
     */
    private $parentContext;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Context", mappedBy="parentContext")
     */
    private $childContexts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Condition", inversedBy="contexts")
     */
    private $conditions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TestGroup", inversedBy="contexts")
     */
    private $testGroupNeeded;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Test", mappedBy="context")
     */
    private $tests;

    public function __construct()
    {
        $this->childContexts = new ArrayCollection();
        $this->conditions = new ArrayCollection();
        $this->testGroupNeeded = new ArrayCollection();
        $this->tests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParentContext(): ?self
    {
        return $this->parentContext;
    }

    public function setParentContext(?self $parentContext): self
    {
        $this->parentContext = $parentContext;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildContexts(): Collection
    {
        return $this->childContexts;
    }

    public function addChildContext(self $childContext): self
    {
        if (!$this->childContexts->contains($childContext)) {
            $this->childContexts[] = $childContext;
            $childContext->setParentContext($this);
        }

        return $this;
    }

    public function removeChildContext(self $childContext): self
    {
        if ($this->childContexts->contains($childContext)) {
            $this->childContexts->removeElement($childContext);
            // set the owning side to null (unless already changed)
            if ($childContext->getParentContext() === $this) {
                $childContext->setParentContext(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Condition[]
     */
    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    public function addCondition(Condition $condition): self
    {
        if (!$this->conditions->contains($condition)) {
            $this->conditions[] = $condition;
        }

        return $this;
    }

    public function removeCondition(Condition $condition): self
    {
        if ($this->conditions->contains($condition)) {
            $this->conditions->removeElement($condition);
        }

        return $this;
    }

    /**
     * @return Collection|TestGroup[]
     */
    public function getTestGroupNeeded(): Collection
    {
        return $this->testGroupNeeded;
    }

    public function addTestGroupNeeded(TestGroup $testGroupNeeded): self
    {
        if (!$this->testGroupNeeded->contains($testGroupNeeded)) {
            $this->testGroupNeeded[] = $testGroupNeeded;
        }

        return $this;
    }

    public function removeTestGroupNeeded(TestGroup $testGroupNeeded): self
    {
        if ($this->testGroupNeeded->contains($testGroupNeeded)) {
            $this->testGroupNeeded->removeElement($testGroupNeeded);
        }

        return $this;
    }

    /**
     * @return Collection|Test[]
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

    public function addTest(Test $test): self
    {
        if (!$this->tests->contains($test)) {
            $this->tests[] = $test;
            $test->setContext($this);
        }

        return $this;
    }

    public function removeTest(Test $test): self
    {
        if ($this->tests->contains($test)) {
            $this->tests->removeElement($test);
            // set the owning side to null (unless already changed)
            if ($test->getContext() === $this) {
                $test->setContext(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
