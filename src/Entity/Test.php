<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $number;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Test", inversedBy="prerequisiteTests")
     */
    private $prerequisite;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Test", mappedBy="prerequisite")
     */
    private $prerequisiteTests;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Context", inversedBy="tests")
     */
    private $context;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestCampaign", mappedBy="test")
     */
    private $testCampaigns;

    public function __construct()
    {
        $this->prerequisite = new ArrayCollection();
        $this->prerequisiteTests = new ArrayCollection();
        $this->testCampaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getPrerequisite(): Collection
    {
        return $this->prerequisite;
    }

    public function addPrerequisite(self $prerequisite): self
    {
        if (!$this->prerequisite->contains($prerequisite)) {
            $this->prerequisite[] = $prerequisite;
        }

        return $this;
    }

    public function removePrerequisite(self $prerequisite): self
    {
        if ($this->prerequisite->contains($prerequisite)) {
            $this->prerequisite->removeElement($prerequisite);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getPrerequisiteTests(): Collection
    {
        return $this->prerequisiteTests;
    }

    public function addPrerequisiteTest(self $prerequisiteTest): self
    {
        if (!$this->prerequisiteTests->contains($prerequisiteTest)) {
            $this->prerequisiteTests[] = $prerequisiteTest;
            $prerequisiteTest->addPrerequisite($this);
        }

        return $this;
    }

    public function removePrerequisiteTest(self $prerequisiteTest): self
    {
        if ($this->prerequisiteTests->contains($prerequisiteTest)) {
            $this->prerequisiteTests->removeElement($prerequisiteTest);
            $prerequisiteTest->removePrerequisite($this);
        }

        return $this;
    }

    public function getContext(): ?Context
    {
        return $this->context;
    }

    public function setContext(?Context $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function __toString()
    {
        return $this->getNumber();
    }

    /**
     * @return Collection|TestCampaign[]
     */
    public function getTestCampaigns(): Collection
    {
        return $this->testCampaigns;
    }

    public function addTestCampaign(TestCampaign $testCampaign): self
    {
        if (!$this->testCampaigns->contains($testCampaign)) {
            $this->testCampaigns[] = $testCampaign;
            $testCampaign->setTest($this);
        }

        return $this;
    }

    public function removeTestCampaign(TestCampaign $testCampaign): self
    {
        if ($this->testCampaigns->contains($testCampaign)) {
            $this->testCampaigns->removeElement($testCampaign);
            // set the owning side to null (unless already changed)
            if ($testCampaign->getTest() === $this) {
                $testCampaign->setTest(null);
            }
        }

        return $this;
    }

}
