<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeatureRepository")
 */
class Feature
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subject", inversedBy="features")
     */
    private $subject;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestGroup", mappedBy="feature")
     */
    private $testGroups;

    public function __construct()
    {
        $this->testGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return Collection|TestGroup[]
     */
    public function getTestGroups(): Collection
    {
        return $this->testGroups;
    }

    public function addTestGroup(TestGroup $testGroup): self
    {
        if (!$this->testGroups->contains($testGroup)) {
            $this->testGroups[] = $testGroup;
            $testGroup->setFeature($this);
        }

        return $this;
    }

    public function removeTestGroup(TestGroup $testGroup): self
    {
        if ($this->testGroups->contains($testGroup)) {
            $this->testGroups->removeElement($testGroup);
            // set the owning side to null (unless already changed)
            if ($testGroup->getFeature() === $this) {
                $testGroup->setFeature(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
