<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 */
class Campaign
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TestSeries", mappedBy="campaign")
     */
    private $testSeries;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TestCampaign", inversedBy="campaign")
     */
    private $testCampaign;

    public function __construct()
    {
        $this->testSeries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    /**
     * @return Collection|TestSeries[]
     */
    public function getTestSeries(): Collection
    {
        return $this->testSeries;
    }

    public function addTestSeries(TestSeries $testSeries): self
    {
        if (!$this->testSeries->contains($testSeries)) {
            $this->testSeries[] = $testSeries;
            $testSeries->addCampaign($this);
        }

        return $this;
    }

    public function removeTestSeries(TestSeries $testSeries): self
    {
        if ($this->testSeries->contains($testSeries)) {
            $this->testSeries->removeElement($testSeries);
            $testSeries->removeCampaign($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getTestCampaign(): ?TestCampaign
    {
        return $this->testCampaign;
    }

    public function setTestCampaign(?TestCampaign $testCampaign): self
    {
        $this->testCampaign = $testCampaign;

        return $this;
    }
}
